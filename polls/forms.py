from django import forms
from django.contrib.auth.models import User
from .models import Question
from .models import Choice
class Question_forms(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('question_text',)

class Choice_forms(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ('question','choice_text',)

