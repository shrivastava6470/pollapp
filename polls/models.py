from django.db import models
from django.contrib.auth.models import User

# Create your models here.



class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.question_text


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    updateby=models.ForeignKey(User,on_delete=models.CASCADE)
    createby=models.ForeignKey(User,on_delete=models.CASCADE,related_name='createby')
    def __str__(self):
        return self.choice_text
class votegiven(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    vote_givenby=models.ForeignKey(User,on_delete=models.CASCADE,related_name='vote_givenby')
    def __str__(self):
        return self.question.question_text