from django.contrib import admin
from django.urls import path,include
from polls import views
from django.urls import path
from django.contrib.auth.views import LoginView

from . import views
from  polls.views import SearchResultsView

urlpatterns = [
    # ex: /polls/
    path('',views.xyz,name ='xyz'),
    path('123/', views.index, name='index'),

    # ex: /polls/5/
    path('<int:question_id>/', views.detail, name='detail'),
    path('temp/', views.temp, name='temp'),
   # path('login', views.login, name='login'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', views.logout, name='logout'),


    # ex: /polls/5/results/
    path('<int:question_id>/results/', views.results, name='results'),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('search/', SearchResultsView.as_view(), name='search_results'),

]
'''
urlpatterns = [

    path('',views.index)
]
'''